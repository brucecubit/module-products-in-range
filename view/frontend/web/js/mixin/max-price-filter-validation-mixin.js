/*
 * Copyright (c) 2021.
 * @author Bruce Cubit
 * @category Magento2
 * @file: max-price-filter-validation-mixin.js
 * @last modified: 1/21/21, 2:29 PM
 */

define(['jquery'], function($) {
    'use strict';
    return function() {
        $.validator.addMethod(
            'validate-products-in-range-max',
            function(value, element, minElement) {
                let max = (Number($(minElement).val()) * 5),
                    min = Number($(minElement).val()),
                    val = Number(value),
                    msg = $.mage.__(
                    "Please enter a value between %1 and %2"
                ).replace('%1', min).replace('%2', max);
                if(val > min && val <= max) {
                    $(element).data('msg-validate-products-in-range-max', '');
                    return true;
                }
                if(min < 0) {
                    let msg = 'The "Lowest Product Price" field must be a number greater than 0';
                    $(element).data('msg-validate-products-in-range-max', msg);
                    return false;
                }
                $(element).data('msg-validate-products-in-range-max', msg);
                return false;
            },
        )
    }
});
