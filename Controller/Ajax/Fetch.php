<?php

/*
 * Copyright (c) 2021.
 * @author Bruce Cubit
 * @category Magento2
 * @file: Products.php
 * @last modified: 1/19/21, 6:07 PM
 */
/** @noinspection ContractViolationInspection */
/** getRequest() is added as a public method for back compatibility
 * @see Magento\Framework\App\Action\Action
 */

namespace BruceCubit\ProductsInRange\Controller\Ajax;

use BruceCubit\ProductsInRange\Model\Product\Filter\DataProvider;
use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Fetch
 * @package BruceCubit\ProductsInRange\Controller\Ajax
 */
class Fetch implements HttpGetActionInterface
{
    public const REQUEST_MIN_PRICE_KEY = 'minPrice';
    public const REQUEST_MAX_PRICE_KEY = 'maxPrice';
    public const REQUEST_SORT_ORDER_KEY = 'sortOrder';

    /**
     * @var DataProvider
     */
    private DataProvider $dataProvider;

    /**
     * @var RequestInterface
     */
    private RequestInterface $request;
    /**
     * @var ResultFactory
     */
    private ResultFactory $resultFactory;
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * Fetch constructor.
     * @param DataProvider $dataProvider
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     * @param SerializerInterface $serializer
     */
    public function __construct(
        DataProvider $dataProvider,
        RequestInterface $request,
        ResultFactory $resultFactory,
        SerializerInterface $serializer
    ) {
        $this->dataProvider = $dataProvider;
        $this->request = $request;
        $this->resultFactory = $resultFactory;
        $this->serializer = $serializer;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        /** @var Json $resultJSON */
        $resultJSON = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if (!$this->isValidRequest()) {
            return $resultJSON->setData([])->setHttpResponseCode(400);
        }
        $params = $this->getRequest()->getParams();
        $products = $this->dataProvider
            ->getProducts(
                $params[self::REQUEST_MIN_PRICE_KEY],
                $params[self::REQUEST_MAX_PRICE_KEY],
                $params[self::REQUEST_SORT_ORDER_KEY]
            );
        return $resultJSON->setData($products)->setHttpResponseCode(200);
    }

    /**
     * @return RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    protected function isValidRequest(): bool
    {
        return $this->requestHasValidKeys() && $this->requestHasValidValues();
    }

    private function requestHasValidKeys(): bool
    {
        $params = $this->getRequest()->getParams();
        return array_key_exists(self::REQUEST_MIN_PRICE_KEY, $params)
            && array_key_exists(self::REQUEST_MAX_PRICE_KEY, $params)
            && array_key_exists(self::REQUEST_SORT_ORDER_KEY, $params);
    }

    private function requestHasValidValues(): bool
    {
        $params = $this->getRequest()->getParams();
        return $params[self::REQUEST_MIN_PRICE_KEY] > 0
            && $params[self::REQUEST_MAX_PRICE_KEY] <= ($params[self::REQUEST_MIN_PRICE_KEY] * 5);
    }
}
