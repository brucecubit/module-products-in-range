#ProductsInRange
### Crimson Agility Skill Assessment
- Customer Account Section Magento 2.3 Stand Alone
    - 1: Products Page (**New Tab**)
        - **FORM** Based On Customer Input Range of Prices Entered (3 Input Fields)
            - [x] Low Price
                - [x] Required
                - [x] Number ("Any Value" > 0)
            - [x] High Price
                - [x] Required
                - [x] Greater Than Than Low Price
                - [x] Less Than 5x Low Price )if Low is 10 , Max here would be 50) (CUSTOM VALIDATION METHOD)
            - [x] Sort (Meaning Sort Order)
                - [x] Ascending (Default)
                - [x] Descending
            - [x] On Form Submit
                - [x] Fire AJAX
                    - [x] Return List of Products within that range **If**:
                        - [x] Front end form passes Java Script Validation (Use Mage Validation)
                        - [x] Back end controller validation (Required Form Fields Exist in request)
                          *!!!Do not process grid without validating in back end controller.*
    - 2: On Successful submission form
        - [x] Render Product Table (Grid)
            - [x] First Ten Products
            - [x] Sorted By Price Based On Input Sort Order
            - [x] Following Columns
                - [x] Thumbnail
                - [x] SKU
                - [x] Name
                - [x] QTY
                - [x] Price
                - [x] Link To Product Page (Open in new tab)
	
