/*
 * Copyright (c) 2021.
 * @author Bruce Cubit
 * @category Magento2
 * @file: list.js
 * @last modified: 1/20/21, 11:10 AM
 */

define(
    [
        'ko',
    ],
    function (
        ko,
    ) {
        'use strict';
        return {
            list: ko.observableArray([])
        };
    }
);
