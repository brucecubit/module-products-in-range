
define([
        'jquery',
        'uiComponent',
        'ko',
        'mage/storage',
        'BruceCubit_ProductsInRange/js/model/product',
    ], function ($, Component, ko, storage, products) {
        'use strict';
        return {
            search: function(requestUrl, minPrice, maxPrice, sortOrder) {
                $.ajax({
                    type: 'GET',
                    url: requestUrl + '?minPrice=' + minPrice + '&maxPrice=' + maxPrice + '&sortOrder=' + sortOrder,
                    data: {
                        'form_key': window.FORM_KEY
                    },
                    showLoader: true,
                    dataType: 'json',

                    /**
                     * Resolve with the response result
                     *
                     * @param {Object} response
                     */
                    success: function (response) {
                        products.list(response);
                    },

                    /**
                     * Reject with the message from response
                     *
                     * @param {Object} response
                     */
                    error: function (response) {
                        products.list([])
                    }
                });
            }
        };
    }
);
