<?php
/*
 * Copyright (c) 2021.
 * @author Bruce Cubit
 * @category Magento2
 * @file: View.php
 * @last modified: 1/20/21, 1:07 PM
 */

namespace BruceCubit\ProductsInRange\Controller\Customer;

use Magento\Customer\Model\Session;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;

class View implements ActionInterface
{
    protected PageFactory $resultPageFactory;
    protected Session $session;
    /**
     * @var RedirectFactory
     */
    private RedirectFactory $redirectFactory;

    /**
     * Catalog constructor.
     * @param PageFactory $resultPageFactory
     * @param Session $session
     * @param RedirectFactory $redirectFactory
     * @noinspection InterfacesAsConstructorDependenciesInspection
     */
    public function __construct(PageFactory $resultPageFactory, Session $session, RedirectFactory $redirectFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->session = $session;
        $this->redirectFactory = $redirectFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        if (!$this->session->isLoggedIn()) {
            return $this->redirectFactory
                ->create()->setPath('customer/account/login')
                ->setHttpResponseCode(401);
        }
        return $this->resultPageFactory->create();
    }
}
