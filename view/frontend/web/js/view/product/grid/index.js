
/*
 * Copyright (c) 2021.
 * @author Bruce Cubit
 * @category Magento2
 * @file: index.js
 * @last modified: 1/20/21, 11:11 AM
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'BruceCubit_ProductsInRange/js/model/product',
], function ($, ko, Component, products) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'BruceCubit_ProductsInRange/product/grid/index'
        },
        products       : ko.observableArray([]),
        isVisible      : ko.observable(false),
        initialize: function () {
            this._super();
            let self = this;
            products.list.subscribe(function(p){
                self.products(p);
            });
        }
    });
});
