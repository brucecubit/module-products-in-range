
/*
 * Copyright (c) 2021.
 * @author Bruce Cubit
 * @category Magento2
 * @file: filter.js
 * @last modified: 1/20/21, 11:58 AM
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'BruceCubit_ProductsInRange/js/action/product',
    'BruceCubit_ProductsInRange/js/model/product',
], function ($, ko, Component, product, products) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'BruceCubit_ProductsInRange/product/grid/index'
        },
        minPrice       : ko.observable(),
        maxPrice       : ko.observable(),
        fetchUrl     : '',
        selectedSortOrder       : ko.observable('ASC'),
        isVisible       : ko.observable(true),
        showClearBtn       : ko.observable(false),
        sortOrders      : ko.observableArray([
            {text: 'ASC', value: 'ASC'},
            {text: 'DESC', value: 'DESC'}
        ]),
        initialize: function (data) {
            this._super();
            this.fetchUrl = data.fetchUrl;
        },
        reset: function() {
            this.isVisible(true);
            this.showClearBtn(false);
            this.minPrice(null);
            this.maxPrice(null);
            products.list([]);
            this.selectedSortOrder('ASC');
        },
        beforeSearch: function(e) {
            if($(e).valid()) {
                this.search();
            }
            return false;
        },
        search: function() {
           this.showClearBtn(true);
           this.isVisible(false);
            product.search(
                this.fetchUrl,
                this.minPrice(),
                this.maxPrice(),
                this.selectedSortOrder()
            );
        }
    });
});
