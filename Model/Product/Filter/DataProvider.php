<?php
/*
 * Copyright (c) 2021.
 * @author Bruce Cubit
 * @category Magento2
 * @file: DataProvider.php
 * @last modified: 1/19/21, 6:43 PM
 */

namespace BruceCubit\ProductsInRange\Model\Product\Filter;

use Magento\Catalog\Helper\ImageFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\Pricing\Helper\DataFactory;

/**
 * Provides a filtered list of products
 * Class DataProvider
 * @package BruceCubit\ProductsInRange\Model\Product\Filter
 */
class DataProvider
{
    private CollectionFactory $productCollectionFactory;

    private ImageFactory $imageFactory;

    private Data $priceHelper;


    /**
     * DataProvider constructor.
     * @param CollectionFactory $productCollectionFactory
     * @param ImageFactory $imageFactory
     * @param DataFactory $priceHelperFactory
     */
    public function __construct(
        CollectionFactory $productCollectionFactory,
        ImageFactory $imageFactory,
        DataFactory $priceHelperFactory
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->imageFactory = $imageFactory;
        $this->priceHelper = $priceHelperFactory->create();
    }

    /**
     * @param $lowestPrice
     * @param $highestPrice
     * @param string $sort
     * @param int $limit
     * @return array
     */
    public function getProducts($lowestPrice, $highestPrice, $sort = 'asc', $limit = 10): array
    {
        $products = [];
        $collection = $this->productCollectionFactory->create()
            ->addFieldToSelect('name')
            ->addFieldToSelect('qty')
            ->addFieldToSelect('final_price')
            ->addFieldToSelect('thumbnail')
            ->addFieldToSelect('url_key')
            ->addFieldToFilter('price', ['gt' => 0])
            ->addFieldToFilter('price', ['gt' => 0])
            ->addFieldToFilter('price', ['from' => $lowestPrice, 'to' => $highestPrice])
            ->addAttributeToFilter('visibility', ['eq' => 1])
            ->addAttributeToFilter("is_saleable", 1)
            ->setPageSize($limit)
            ->setOrder('price', strtoupper($sort));
        $joinConditions = 'p.product_id = e.entity_id';

        if (isset($collection)) {
            $collection->getSelect()->join(
                ['p' => $collection->getTable('cataloginventory_stock_item')],
                $joinConditions,
                ['qty']
            );
        }
        foreach ($collection->getItems() as $item) {
            /** @var Product $item */
            $imageUrl = $this->imageFactory->create()
                ->init($item, 'product_thumbnail_image')->getUrl();
            $item = $item->setData('thumbnail', $imageUrl);
            $item = $item->setData('product_url', $item->getProductUrl());
            $item = $item->setData('qty', number_format($item->getQty(), '0', '.', ''));
            $item = $item->setData('price', $this->priceHelper->currency($item->getData('price'), true, false));
            $products[] = $item->getData();
        }
        return $products;
    }
}
