/*
 * Copyright (c) 2021.
 * @author Bruce Cubit
 * @category Magento2
 * @file: requirejs-config.js
 * @last modified: 1/21/21, 2:27 PM
 */

let config = {
    config: {
        mixins: {
            'mage/validation': {
                'BruceCubit_ProductsInRange/js/mixin/max-price-filter-validation-mixin': true
            }
        }
    }
}
